package com.vision_games.arrebentando_seumestre;

public class NotebookEntity extends EnemyEntity
{
	private Game game;
	private String opened;
	private int count;
	private boolean attacked;
	
	public NotebookEntity(Game game,int x,int y)
	{
		super(game,"resource/image/enemies/caderno0.png",x,y);
		
		this.y = 650-sprite.getHeight();
		
		this.game = game;
		opened = "resource/image/enemies/caderno1.png";

		count = 0;
		life = 2;
		attack = 1;
		max_speed_x = 50.0;
		
		moveLeft();
	}
	
	public void move(long delta)
	{
		super.move(delta);
	}
	
	public void takeDamage(int d)
	{
		life -= d;

		if(life == 1)
		{
			this.dx = 0;
			sprite.loadImage(opened);
		}
		else if(life == 0)
		{
			game.saveGame();
			game.removeEntity(this);
		}
	}

	public void collidedWith(Entity other)
	{
		if(other instanceof HeroEntity)
		{
			if(((HeroEntity) other).getAttack() == true)
				this.takeDamage(((HeroEntity) other).getDamage());
			else
			{
				if(attacked == false)
				{
					attacked = true;
					
					if(life == 2)
						((HeroEntity) other).takeDamage(attack);
				}
			}
			
			if(life == 2)
			{
				this.moveRight();
				count = 200;
				game.updateLogic();
			}
		}
	}
	
	@Override
	public void doLogic()
	{
		if(count == 0)
		{
			attacked = false;
			this.moveLeft();//this.setHorizontalMovement(-300.0f*0.2f);
		}
		else
		{
			game.updateLogic();
			--count;
		}
	}
}