package com.vision_games.arrebentando_seumestre;

import java.awt.Graphics;

/**
 * A entidade que representa o jogador
 * 
 * @author Kevin Glass
 */
public class HeroEntity extends Entity
{
	/** O jogo no qual o jogador existe */
	private Game game;

	private String rest_sprite = "resource/image/player/player.png";
	private String attack_sprite = "resource/image/player/attack.png";
	
	private LevelBar life_bar;
	private LevelBar coffee_bar;
	private LevelBar sleep_imunity;
	private LevelBar dp;
	
	private boolean attacking;
	private boolean attacked;
	private boolean sleep_effect;
	private int attack;
	private int dps;	// lifes
	
	/**
	 * Cria uma nova entidade que representa o jogador
	 *  
	 * @param game The game in which the ship is being created
	 * @param ref The reference to the sprite to show for the ship
	 * @param x The initial x location of the player's ship
	 * @param y The initial y location of the player's ship
	 */
	public HeroEntity(Game game,int x,int y)
	{
		super("resource/image/player/player.png",x,y);
		
		this.y = 650-sprite.getHeight();

		this.game = game;
		
		life_bar = new LevelBar(game,"resource/image/player/life.png","resource/image/player/life_empty.png",20,20,10);
		coffee_bar = new LevelBar(game,"resource/image/player/hud_cafe_cheio.png","resource/image/player/hud_cafe_vazio.png",20,660,4);
		sleep_imunity = new LevelBar(game,"resource/image/player/energy.png","",20,80,4);
		dp = new LevelBar(game,"resource/image/player/life_empty.png","",300,80,3);
		
		//coffee_bar.setLevel(0);
		//sleep_imunity.setLevel(0);
		dp.setLevel(0);
		
		sleep_effect = false;
		attacking = false;
		attacked = false;
		attack = 1;
		max_speed_x = 200.0;
		dps = 0;
	}
	
	public void setAttack(boolean a)
	{
		attacking = a;
		
		if(attacking == true)
		{
			if(sprite.isFlipped() == true)
			{
				attacking = false;
			}
			else
			{
				sprite.loadImage(attack_sprite);
			}
		}
		else
		{
			attacked = false;
			sprite.loadImage(rest_sprite);
		}
	}
	
	public boolean getAttack()
	{
		return attacking;
	}
	
	public void setSleepy(boolean se)
	{
		if(sleep_imunity.getLevel() > 0)
		{
			sleep_imunity.downLevel();
			return;
		}
		
		if(se == true)
			this.dx *= 0.40;
		else
			this.dx /= 0.40;
		
		sleep_effect = se;
	}
	
	public boolean getSleepy()
	{
		return sleep_effect;
	}
	
	public int getDamage()
	{
		if(attacked == false)
		{
			attacked = true;
			return attack;
		}
		else
			return 0;
	}
	
	public void takeDamage(int d)
	{
		if(attacked == true) return;

		life_bar.setLevel(life_bar.getLevel()-d);

		if(life_bar.getLevel() < 6)
		{
			++dps;
			life_bar.setLevel(10);
			
			dp.upLevel();
			game.dp();
		}

		if(dps > 2)
			game.jubilado();
	}

	public void drinkCoffee()
	{
		if(coffee_bar.getLevel() > 0)
		{
			coffee_bar.downLevel();
			
			if(sleep_effect == true)
				sleep_effect = false;
			else
				sleep_imunity.upLevel();
		}
	}
	
	public int getEnergy()
	{
		sleep_imunity.downLevel();
		return sleep_imunity.getLevel();
	}
	
	/**
	 * Rquisita que o heroi se mova baseado numa quantidade de tempo
	 * decorrido
	 * 
	 * @param delta O tempo que passou desde o ultimo movimento (ms)
	 */
	public void move(long delta)
	{
		// Se esta movendo para esquerda e chegou no lado esquerdo
		// da tela, nao se move
		if ((dx < 0) && (x < 10))
			return;

		// Se esta movendo para direita e chegou no lado direito
		// da tela, nao se move
		if ((dx > 0) && (x > (1280-sprite.getWidth())))
			return;
		
		super.move(delta);
	}
	
	/**
	 * Seta a velocidade horizontal dessa entidade
	 * 
	 * @param dx A velocidade horizontal dessa entidade (pixels/sec)
	 */
	@Override
	public void setHorizontalMovement(double dx)
	{
		if(sleep_effect == false)
			this.dx = dx;
		else
			this.dx = dx * 0.40;
		
		if(dx < 0)
			sprite.setFlipped(true);
		else if (dx > 0)
			sprite.setFlipped(false);
	}
	
	@Override
	public void moveRight()
	{
		this.setHorizontalMovement(max_speed_x);
	}
	
	@Override
	public void moveLeft()
	{
		this.setHorizontalMovement(-max_speed_x);
	}
	
	@Override
	public void doLogic()
	{}
	
	public void draw(Graphics g)
	{
		super.draw(g);
		
		life_bar.draw(g);
		coffee_bar.draw(g);
		sleep_imunity.draw(g);
		dp.draw(g);
	}
	
	/**
	 * Notificacao de que o player colidiu com algo
	 * 
	 * @param other A entidade com a qual o player colidiu
	 */
	public void collidedWith(Entity other){}
}