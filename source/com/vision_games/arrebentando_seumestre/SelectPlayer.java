package com.vision_games.arrebentando_seumestre;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class SelectPlayer {
	private ArrayList<String> user_list;
	private final int list_max = 10;
	private int in_highlight;
	private int highlight;
	private int next_highlight;
	private int cursor = 0;
	private Graphics2D g;
	private Game game;
	
	private boolean selected = false;
	
	public SelectPlayer(Game game, ArrayList<String> users){
		user_list = users;
		
		this.game = game;
		this.game.addKeyListener(new KeySelectInputHandler());
		
		if((user_list.size() % 2) != 0)
			in_highlight = (user_list.size()+1)/2;
		else
			in_highlight = (user_list.size())/2;
	}
	
	public boolean showList(Graphics2D g){
		this.g = g;

		g.setColor(Color.black);
		g.setFont(new Font("Default",Font.PLAIN,40));
		g.drawString("Pressione Enter para selecionar um jogador",(1280-g.getFontMetrics().stringWidth("Pressione Enter para selecionar um jogador"))/2,40);
		g.drawString("Pressione N para criar um novo usuario",(1280-g.getFontMetrics().stringWidth("Pressione N para criar um novo usuario"))/2,80);
		g.drawString("Pressione D para excluir perfil",(1280-g.getFontMetrics().stringWidth("Pressione D para excluir perfil"))/2,120);
		for(int i = 0; i < user_list.size(); ++i){
			if(i < list_max){
				if(in_highlight == i+1){
					highlight = 30;
				}
				else{
					highlight = 0;
				}
				
				if(i >= in_highlight){
					next_highlight = 40;
				}
				else{
					next_highlight = 0;
				}
				
				g.setColor(Color.black);
				g.setFont(new Font("Default",Font.PLAIN,20+highlight));
				
				String message;
				if((i+cursor) > (user_list.size()-1)){
					message = user_list.get(cursor+i-user_list.size());
				}
				else{
					message = user_list.get(cursor+i);
				}
				g.drawString(message,(1280-g.getFontMetrics().stringWidth(message))/2-highlight,150+(i*20)+highlight+next_highlight);
				
			}
		}
		return selected;
		
	}
	
	private class KeySelectInputHandler extends KeyAdapter
	{		
		/**
		 * Notificacao do AWT que uma tecla foi pressionada. Note que
		 * uma tecla ser pressionada significa que foi 'empurrada pra baixo', mas *NAO*
		 * solta. Ai que keyTyped() entra.
		 *
		 * @param e Detalhes da tecla que foi pressionada
		 */
		public void keyPressed(KeyEvent e)
		{			
			if (e.getKeyCode() == KeyEvent.VK_S){
				if(cursor == 0)
					cursor = list_max-1;
				else
					cursor -= 1;
			}
			else if (e.getKeyCode() == KeyEvent.VK_W){
				if(cursor == list_max-1)
					cursor = 0;
				else
					cursor += 1;
			}
			else if (e.getKeyCode() == KeyEvent.VK_ENTER){
				selected = true;
				game.removeKeyListener(this);
			}
			else if (e.getKeyCode() == KeyEvent.VK_D){
				
			}
		} 
		
		/**
		 * Notificacao do AWT que uma tecla foi liberada.
		 *
		 * @param e Detalhes da tecla que foi liberada
		 */
		public void keyReleased(KeyEvent e)
		{
		}

		/**
		 * Notificacao do AWT que uma teclado foi usada. Note que
		 * usar uma tecla significa que foi pressionada e liberada.
		 *
		 * @param e Detalhes da tecla usada
		 */
		public void keyTyped(KeyEvent e) {			
			// se pressionar ESC, sai do jogo
			if (e.getKeyChar() == 27)
			{
				System.exit(0);
			}
		}
	}
}
