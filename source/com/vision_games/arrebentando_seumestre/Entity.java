package com.vision_games.arrebentando_seumestre;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * An entity represents any element that appears in the game. The
 * entity is responsible for resolving collisions and movement
 * based on a set of properties defined either by subclass or externally.
 * 
 * Note that doubles are used for positions. This may seem strange
 * given that pixels locations are integers. However, using double means
 * that an entity can move a partial pixel. It doesn't of course mean that
 * they will be display half way through a pixel but allows us not lose
 * accuracy as we move.
 * 
 * @author Kevin Glass
 */
public abstract class Entity
{
	/** A posicao x atual dessa entidade */ 
	protected double x, start_x;
	/** A posicao y atual dessa entidade */ 
	protected double y, start_y;
	/** O sprite que representa essa entidade */
	protected Sprite sprite;
	/** A velocidade horizontal atual dessa entidade (pixels/sec) */
	protected double dx;
	protected double max_speed_x;
	/** A velocidade vertical atual dessa entidade (pixels/sec) */
	protected double dy;
	protected double max_speed_y;
	/** O retangulo usado para essa entidade durante a solucao das colisoes */
	private Rectangle me = new Rectangle();
	/** O retangulo usado para outras entidades durante a solucao das colisoes */
	private Rectangle him = new Rectangle();
	
	/**
	 * Constroi a entidade baseado em uma imagem e posicao.
	 * 
	 * @param ref A referencia para uma imagem a ser mostrada para essa entidade
 	 * @param x A posicao x inicial dessa entidade
	 * @param y A posicao y inicial dessa entidade
	 */
	public Entity(String ref,int x,int y)
	{
		this.sprite = new Sprite(ref);
		
		this.x = (double)x;
		this.y = (double)y;
		
		start_x = this.x;
		start_y = this.y;
		
		max_speed_x = 0.0;
		max_speed_y = 0.0; 
	}
	
	/**
	 * Requisita que essa entidade se mova baseada numa certa quantidade
	 * de tempo decorrido.
	 * 
	 * @param delta A quantidade de tempo que passou em milisegundos
	 */
	public void move(long delta)
	{
		// atualiza a posicao da entidade baseado na velocidade
		x += (delta * dx) / 1000;
		y += (delta * dy) / 1000;
	}
	
	public void resetPos()
	{
		this.x = start_x;
		this.y = start_y;
		
		this.setHorizontalMovement(0);
	}
	
	/**
	 * Seta a velocidade horizontal dessa entidade
	 * 
	 * @param dx A velocidade horizontal dessa entidade (pixels/sec)
	 */
	public abstract void setHorizontalMovement(double dx);

	/**
	 * Seta a velocidade vertical dessa entidade
	 * 
	 * @param dx A velocidade vertical dessa entidade (pixels/sec)
	 */
	public void setVerticalMovement(double dy)
	{
		this.dy = dy;
	}
	
	/**
	 * Pega a velocidade horizontal dessa entidade
	 * 
	 * @return A velocidade horizontal dessa entidade (pixels/sec)
	 */
	public double getHorizontalMovement()
	{
		return dx;
	}

	/**
	 * Pega a velocidade vertical dessa entidade
	 * 
	 * @return A velocidade vertical dessa entidade (pixels/sec)
	 */
	public double getVerticalMovement()
	{
		return dy;
	}
	
	public void moveLeft()
	{
		dx = -max_speed_x;
	}
	
	public void moveRight()
	{
		dx = max_speed_x;
	}
	
	
	/**
	 * Desenha essa entidade no contexto grafico dado
	 * 
	 * @param g O contexto grafico no qual deve desenhar
	 */
	public void draw(Graphics g)
	{
		sprite.draw(g,(int) x,(int) y);
	}
	
	/**
	 * Faz a logica associada com essa entidade. Esse metodo
	 * sera chamado periodicamente baseado nos eventos do jogo
	 */
	public abstract void doLogic();
	
	/**
	 * Pega a posicao x dessa entidade
	 * 
	 * @return A posicao x dessa entidade
	 */
	public int getX()
	{
		return (int) x;
	}

	public double getMaxSpeedX()
	{
		return max_speed_x;
	}
	
	/**
	 * Pega a posicao y dessa entidade
	 * 
	 * @return A posicao y dessa entidade
	 */
	public int getY()
	{
		return (int) y;
	}
	
	public int getWidth()
	{
		return sprite.getWidth();
	}
	
	public int getHeight()
	{
		return sprite.getHeight();
	}
	
	
	/**
	 * Verifica se essa entidade colidiu com outra.
	 * 
	 * @param other A outra entidade a verificar se ocorrou a colisao
	 * @return True se as entidade colidiram entre si
	 */
	public boolean collidesWith(Entity other)
	{
		me.setBounds((int) x,(int) y,sprite.getWidth(),sprite.getHeight());
		him.setBounds((int) other.x,(int) other.y,other.sprite.getWidth(),other.sprite.getHeight());

		return me.intersects(him);
	}
	
	/**
	 * Notificacao de que essa entidade colidiu com outra
	 * 
	 * @param other A entidade com a qual essa colidiu
	 */
	public abstract void collidedWith(Entity other);
}