package com.vision_games.arrebentando_seumestre;

public class SleepyEntity extends EnemyEntity
{
	private Game game;
	
	public SleepyEntity(Game game,int x,int y)
	{
		super(game,"resource/image/enemies/sono.png",x,y);
		
		this.y = 650-sprite.getHeight();
		
		this.game = game;

		//count = 0;
		life = 1;
		attack = 0;
		max_speed_x = 20.0;
		
		game.updateLogic();
	}
	
	public void takeDamage(int d)
	{
		life -= d;

		if(life == 0)
			game.removeEntity(this);
	}
	
	public void collidedWith(Entity other)
	{
		if(other instanceof HeroEntity)
		{
			if(((HeroEntity) other).getAttack() == true)
			{
				this.takeDamage(((HeroEntity) other).getDamage());
			}
			else
			{
				((HeroEntity) other).setSleepy(true);

				game.removeEntity(this);
				
			}
		}
	}

	@Override
	public void doLogic()
	{
		this.moveLeft();
	}
}
