package com.vision_games.arrebentando_seumestre;

import java.sql.*;
import java.util.ArrayList;

public class BDControl
{
	private Connection conn;
	private Statement stmt;
	private String current_user;

	private String CRIAR_TABELA_JOGADOR = "CREATE TABLE IF NOT EXISTS JOGADOR " +
											"( ID INTEGER PRIMARY KEY AUTOINCREMENT , " +
											"  USUARIO CHAR(20) NOT NULL, " +
											"  NOTA INT NOT NULL, " +
											"  DPS INT NOT NULL, " +
											"  ENERGIA INT NOT NULL, " +
											"  CAFE INT NOT NULL, " +
											"  INIMIGO_ID INTEGER NOT NULL, "
											+ "FOREIGN KEY(INIMIGO_ID) REFERENCES INIMIGO(ID));";
	
	private String CRIAR_TABELA_INIMIGO = "CREATE TABLE IF NOT EXISTS INIMIGO"
			+ "( ID INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ "DESCRICAO CHAR(30) NOT NULL);";

	public BDControl()
	{
		current_user = null;
		this.inicializa();
	}

	public void inicializa()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:arrebentandoseumestre.db");
			stmt = conn.createStatement();
			stmt.executeUpdate(CRIAR_TABELA_INIMIGO);
			stmt.executeUpdate(CRIAR_TABELA_JOGADOR);
		}
		catch (Exception e)
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public void login(String nome) throws Exception
	{
		ResultSet rs = stmt.executeQuery("SELECT * FROM JOGADOR WHERE USUARIO='" + nome + "';");

		if (rs.next())
		{
			current_user = rs.getString("USUARIO");
		}
		rs.close();

		if (nome.equals(current_user))
		{
			//startGame();
		}
		else
		{
			current_user = nome;
			cadastraUsuario(nome);
		}
	}

	public void cadastraUsuario(String nome) throws Exception
	{
		String sql = "INSERT INTO JOGADOR (USUARIO) VALUES ('" + nome + "');"; 
		stmt.executeUpdate(sql);
	}

	public void finaliza()
	{
		try
		{
			stmt.close();
			conn.close();
		}
		catch (Exception e)
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public ArrayList<String> carregaListaNomes()
	{
		try{
			ArrayList<String> usuario = null;
			ResultSet rs = stmt.executeQuery("SELECT * FROM JOGADOR;");
	
			while (rs.next())
			{
				usuario.add(rs.getString("USUARIO"));
			}
	
			rs.close();
		
			return usuario;
		}
		catch (Exception e)
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		
		return null;
	}

	public void salvaJogo(HeroEntity hero)
	{
		// update
		//String sql = "INSERT INTO JOGADOR (NOME, SENHA) VALUES ('" + nome + "', '" + senha1 + "');";
		//String sql = "UPDATE JOGADOR SET NOTA ='" + Integer.toString(hero.getGrade()) + "' WHERE USUARIO='" + current_user + "';";
		//String sql = "UPDATE JOGADOR SET DPS ='" + Integer.toString(hero.getDPS()) + "' WHERE USUARIO='" + current_user + "';";
		//String sql = "UPDATE JOGADOR SET ENERGIA ='" + Integer.toString(hero.getEnergy()) + "' WHERE USUARIO='" + current_user + "';";
		//String sql = "UPDATE JOGADOR SET CAFE ='" + Integer.toString(hero.getCoffee()) + "' WHERE USUARIO='" + current_user + "';";
		//stmt.executeUpdate(sql);
	}
}