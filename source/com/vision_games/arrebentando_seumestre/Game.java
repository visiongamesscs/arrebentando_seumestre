package com.vision_games.arrebentando_seumestre;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *	Loop principal do jogo, onde toda logica acontece
 */
public class Game extends Canvas
{
	/** Estrategia para usar mudanca de pagina acelerada */
	private BufferStrategy strategy;
	/** Verdadeiro para indicar que o jogo esta rodando */
	private boolean game_running = true;
	/** Controle do banco de dados*/
	BDControl bd_control = new BDControl();	
	/** Lista de todas entidades do jogo */
	private ArrayList<Entity> entities = new ArrayList<Entity>();
	/** Lista das entidades a serem removidas do jogo. Ex.: inimigos derrotados */
	private ArrayList<Entity> remove_list = new ArrayList<Entity>();
	/** Lista com os inimigos que devem aparecer na ordem */
	private ArrayList<Entity> enemy_list = new ArrayList<Entity>();
	private int enemy_index;
	/** Lista de objetos que devem aparecer no cenario */
	private ArrayList<Entity> item_list = new ArrayList<Entity>();
	/** Sprite do logo do jogo */
	private Sprite game_logo;
	/** Entidade do player */
	private Entity hero;
	private Entity article;
	private Entity board;
	private Entity notebook;
	private Entity computer;
	private Entity sono;
	private Entity seumestre;
	/** O background precisa ser um entidade porque vai se mover */
	private Entity background;
	/** A velocidade na qual o player se move (pixels/sec) [Desagradavel ter esse valor aqui, mudar pra dentro da entidade] */
	private double move_speed = 300;
	/** O momento do ultimo ataque */
	private long last_attack = 0;
	/** O intervalo entre os ataques do player (ms) */
	private long attack_interval = 500;
	
	/** Mensagem de quando estiver esperando tecla */
	private String message = "";
	/** True se jogo estiver parado esperando uma tecla ser pressionada */
	private boolean waiting_key = true;
	/** True se a seta para esquerda foi pressionada */
	private boolean left_pressed = false;
	/** True se a seta para direita foi pressionada */
	private boolean right_pressed = false;
	/** True se o player esta atacando */
	private boolean attack_pressed = false;
	/** True se o cafe foi tomado */
	private boolean coffee_drunk = false;
	/** True se a logica do jogo precisa ser aplicada */
	private boolean logic_required = false;
	
	private boolean selecionado = false;
	
	/**
	 * Constroi o jogo e coloca pra rodar
	 */
	public Game()
	{
		// Cria o frame que contem o jogo
		JFrame container = new JFrame("Arrebentando o Seumestre");
		
		// Pega o conteudo do frame e configura resolucao do jogo
		JPanel panel = (JPanel) container.getContentPane();
		panel.setPreferredSize(new Dimension(1280,720));
		panel.setLayout(null);
		
		// Configura o tamanho do canvas e o coloca no conteudo do frame
		setBounds(0,0,1280,720);
		panel.add(this);
		
		// Avisa o AWT para nao repintar o canvas ja que
		// isso sera feito no modo acelerado
		setIgnoreRepaint(true);
		
		// Finalmente, faz a janela visivel
		container.pack();
		container.setResizable(false);
		container.setVisible(true);
		
		// adiciona um listener para responder ao fechamento da janela
		container.addWindowListener
		(
			new WindowAdapter()
			{
				public void windowClosing(WindowEvent e)
				{
					System.exit(0);
				}
			}
		);
		
		// requisita o foco para que os eventos venham para essa janela
		requestFocus();

		// cria uma estrategia de buffering que vai permitir o AWT (?)
		// gerenciar os graficos acelerados
		createBufferStrategy(2);
		strategy = getBufferStrategy();
		
		game_logo = new Sprite("resource/image/game_logo.png");
		
		// inicializa as entidades do jogo para que tudo esteja la no inicio
		initEntities();
	}
	
	/**
	 * Inicia um novo jogo, isso limpa os dados antigos e
	 * cria um novo conjunto.
	 */
	private void startGame()
	{
		// limpa as entidades existentes e inicializa um novo conjunto.
		entities.clear();
		initEntities();
		
		// limpa qualquer configuracao de tecla que exista
		left_pressed = false;
		right_pressed = false;
		attack_pressed = false;
		coffee_drunk = false;
	}
	
	private void initEnemies()
	{
		// Inicializacao dos inimigos
		enemy_index = 0;
		
		board = new BoardEntity(this,background.getWidth()+background.getWidth()/2,0);
		enemy_list.add(board);
		
		sono = new SleepyEntity(this,background.getWidth()+background.getWidth()/2,0);
		enemy_list.add(sono);

		notebook = new NotebookEntity(this,background.getWidth()+background.getWidth()/2,0);
		enemy_list.add(notebook);
		
		computer = new ComputerEntity(this,background.getWidth()+background.getWidth()/2,0);
		enemy_list.add(computer);

		article = new ArticleEntity(this,background.getWidth()+background.getWidth()/2,0);
		enemy_list.add(article);
		
		seumestre = new SeuMestre(this,background.getWidth()+background.getWidth()/2,0);
		enemy_list.add(seumestre);
		
		entities.add(enemy_list.get(enemy_index));
	}
	
	private void resetGamePos()
	{
		entities.removeAll(enemy_list);
		enemy_list.clear();
		
		initEnemies();
	}
	
	/**
	 * Initialise the starting state of the entities (ship and aliens). Each
	 * entitiy will be added to the overall list of entities in the game.
	 */
	private void initEntities()
	{
		// cria o background
		background = new Background(this,"resource/image/scenes/Cenario_alpha.jpg",0,0);
		entities.add(background);
		
		// cria o player
		hero = new HeroEntity(this,background.getWidth()/4,400);
		entities.add(hero);
		
		initEnemies();
	}
	
	/**
	 * Notificacao de uma entidade de que a logica do jogo
	 * deve rodar na proxima oportunidade
	 */
	public void updateLogic()
	{
		logic_required = true;
	}
	
	/**
	 * Remove uma entidade do jogo. A entidade removida nao
	 * sera mais movida ou desenhada.
	 * 
	 * @param entity A entidade que deve ser removida
	 */
	public void removeEntity(Entity entity)
	{
		if(entity instanceof EnemyEntity)
		{
			if(enemy_index+1 < enemy_list.size())
			{
				++enemy_index;
				entities.add(enemy_list.get(enemy_index));
			}
		}
			
		remove_list.add(entity);
	}
	
	/**
	 * Notificacao de que o player jubilou (morreu)
	 */
	public void jubilado()
	{
		message = "Oh no! They got you, try again? (Jubilou)";
		waiting_key = true;
	}
	
	public void saveGame()
	{
		message = "You saved the game";
		//bd_control.salvaJogo(hero,enemy_index);
		//waiting_key = true;
	}

	/**
	 * Notificacao de que o player jubilou (morreu)
	 */
	public void dp()
	{
		this.resetGamePos();
		
		message = "Oh no! You're in DP?";
		//waiting_key = true;
	}
	
	/**
	 * Notificacao que o player ganhou
	 */
	public void notifyWin()
	{
		message = "Well done! You Win!";
		waiting_key = true;
	}

	/**
	 * Tenta atacar com a regua do player. Aqui e uma tentativa
	 * pois e preciso verificar se o player pode atacar nesse momento
	 * Ex.: o player esperou o tempo de cooldown?
	 */
	public void tryToAttack()
	{
		// verifica se esperou suficiente para atacar
		if (System.currentTimeMillis() - last_attack < attack_interval)
		{
			return;
		}
		
		if(((HeroEntity) hero).getAttack() == true)
		{
			attack_pressed = false;
			((HeroEntity) hero).setAttack(false);
			return;
		}
		
		// se esperou tempo suficiente, muda sprite para o ataque, e grava o tempo.
		((HeroEntity) hero).setAttack(true);
		
		if(((HeroEntity) hero).getAttack() == false) attack_pressed = false;
		
		last_attack = System.currentTimeMillis();
	}
	
	/**
	 * Loop principal do jogo. Esse loop roda durante todo o jogo
	 * e fica responsavel pelas seguintes atividades:
	 * <p>
	 * - Descobre a velocidade do loop para atualizar movimentos
	 * - Movimenta as entidades do jogo
	 * - Desenha os conteudos da tela (entidades, textos)
	 * - Atualiza eventos do jogo
	 * - Verifica Input
	 * <p>
	 */
	public void gameLoop()
	{
		long loop_time = System.currentTimeMillis();
		
		ArrayList<String> arlist = new ArrayList<String>();
		arlist.add("test1");
		arlist.add("test2");
		arlist.add("test3");
		arlist.add("test4");
		arlist.add("test5");
		arlist.add("test6");
		arlist.add("test7");
		arlist.add("test8");
		arlist.add("test9");
		arlist.add("test10");
		arlist.add("test11");
		
		SelectPlayer select = new SelectPlayer(this,arlist/*bd_control.carregaListaNomes()*/);
		
		// fica no loop ate o jogo acabar
		while (game_running)
		{
			// verifica quanto tempo passou desde a ultima atualizacao, isso
			// vai ser usado para calcular quao longe as entidades devem
			// mexer nesse loop
			long delta = System.currentTimeMillis() - loop_time;
			loop_time = System.currentTimeMillis();

			// Pega o contexto grafico para a superficie
			// acelerada e limpa o conteudo
			Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
			
			g.setColor(Color.black);
			g.fillRect(0,0,1280,720);
						
			// faz um loop para mover todas entidades
			if (!waiting_key)
			{
				for (int i=0;i<entities.size();++i)
				{
					Entity entity = (Entity) entities.get(i);
					
					entity.move(delta);
				}
			}
			
			// faz um loop para desenhar todas entidades do jogo
			for (int i=0;i<entities.size();++i)
			{
				Entity entity = (Entity) entities.get(i);
				
				entity.draw(g);
			}
			
			// compara toda entidade com toda outra entidade
			// Se qualquer uma colidir notifica ambas que ocorreu
			if(!waiting_key)
			{
				for (int p=0;p<entities.size();p++)
				{
					for (int s=p+1;s<entities.size();s++)
					{
						Entity me = (Entity) entities.get(p);
						Entity him = (Entity) entities.get(s);
						
						if (me.collidesWith(him))
						{
							me.collidedWith(him);
							him.collidedWith(me);
						}
					}
				}
			}
			
			// remove toda entidade que foi marcada para retirada
			entities.removeAll(remove_list);
			remove_list.clear();

			// se um elemento do jogo indicou que a logica deve
			// ser executada, testa todoas entidades para ver se
			// a propria logica deve ser executada
			if (logic_required)
			{
				// Precisa ser antes, porque dentro da logica eu posso querer rodar de novo
				logic_required = false;
				
				for (int i=0;i<entities.size();++i)
				{
					Entity entity = (Entity) entities.get(i);
					entity.doLogic();
				}
			}
			

			if(!selecionado){
				selecionado = select.showList(g);
				
				if(selecionado){
					
					// adiciona um key input para o canvas
					// para poder responder a teclas pressionadas
					addKeyListener(new KeyInputHandler());
				}
			}
			
			// se esta aguardando alguma tecla
			// entao desenha a mensagem atual 
			if (waiting_key && selecionado)
			{
				game_logo.draw(g, 1280/2-game_logo.getWidth()/2, 250);
				
				g.setColor(Color.black);
				g.setFont(new Font("Default",Font.PLAIN,20));
				g.drawString(message,(1280-g.getFontMetrics().stringWidth(message))/2,20);
				//if(bd_control.hasSaved() == false)
					g.drawString("Pressione qualquer tecla para come�ar",(1280/2-g.getFontMetrics().stringWidth("Pressione qualquer tecla para come�ar")/2),250+game_logo.getHeight());
				//else
					//g.drawString("Pressione S para come�ar de onde parou, ou N para iniciar um novo jogo",(1280/2-g.getFontMetrics().stringWidth("Pressione S para come�ar de onde parou, ou N para iniciar um novo jogo")/2),250+game_logo.getHeight());
			}
			
			// finalmente, ja foi tudo desenhado, entao limpa os graficos
			// e troca o buffer de desenho
			g.dispose();
			strategy.show();
			
			// resolve movimento do player/background. Primieiro assume que esta
			// tudo parado. Se qualquer direcional estiver pressionado, entao
			// atualiza o movimento de forma apropriada
			hero.setHorizontalMovement(0);
			background.setHorizontalMovement(0);
			// teste
			//article.setHorizontalMovement(-(move_speed*0.2f));
			{
				EnemyEntity enemy = (EnemyEntity)enemy_list.get(enemy_index);
				enemy.correctSpeed(false);
			}
			
			// se o ataque esta pressionado, tenta atacar
			if (attack_pressed)
			{
				tryToAttack();
			}
			else
			{
				if ((left_pressed) && (!right_pressed))
				{
					((HeroEntity)hero).moveLeft();//hero.setHorizontalMovement(-move_speed);
				}
				else if ((right_pressed) && (!left_pressed))
				{
					if(hero.getX() <= background.getWidth()/2-hero.getWidth()/2)
					{
						((HeroEntity)hero).moveRight();//hero.setHorizontalMovement(move_speed);
					}
					else
					{
						background.moveLeft();
						EnemyEntity enemy = (EnemyEntity)enemy_list.get(enemy_index);
						enemy.correctSpeed(true);
						//article.setHorizontalMovement(article.getHorizontalMovement()+background.getHorizontalMovement());
					}
				}
				
				if(coffee_drunk == true)
				{
					((HeroEntity)hero).drinkCoffee();
					coffee_drunk = false;
				}
			}
			
			// finalmente para um pouco. Nota: isso deve rodar por volta de
			// 100 fps mas no windows isso pode variar um pouco a cada loop devido a
			// ma implementacao do timer
			try { Thread.sleep(10); } catch (Exception e) {}
		}
	}
	
	private class KeyInputHandler extends KeyAdapter
	{
		/** Numero de vezes que uma tecla foi pressionada enquanto esperava pressionar alguma (espaco) */
		private int press_count = 1;
		
		/**
		 * Notificacao do AWT que uma tecla foi pressionada. Note que
		 * uma tecla ser pressionada significa que foi 'empurrada pra baixo', mas *NAO*
		 * solta. Ai que keyTyped() entra.
		 *
		 * @param e Detalhes da tecla que foi pressionada
		 */
		public void keyPressed(KeyEvent e)
		{
			// se esta aguardando qualquer tecla entao nao
			// tem nada a ser feito so com o pressionamento
			if (waiting_key)
			{
				return;
			}
			
			
			if (e.getKeyCode() == KeyEvent.VK_A)
				left_pressed = true;
			if (e.getKeyCode() == KeyEvent.VK_D)
				right_pressed = true;
			if (e.getKeyCode() == KeyEvent.VK_SPACE)
				attack_pressed = true;
			if (e.getKeyCode() == KeyEvent.VK_C)
				coffee_drunk = true;
		} 
		
		/**
		 * Notificacao do AWT que uma tecla foi liberada.
		 *
		 * @param e Detalhes da tecla que foi liberada
		 */
		public void keyReleased(KeyEvent e)
		{
			// se esta aguardando qualquer tecla entao nao
			// tem nada a ser feito so com a liberacao
			if (waiting_key)
			{
				return;
			}
			
			if (e.getKeyCode() == KeyEvent.VK_A)
				left_pressed = false;
			if (e.getKeyCode() == KeyEvent.VK_D)
				right_pressed = false;
			//if (e.getKeyCode() == KeyEvent.VK_SPACE)
				//attack_pressed = false;
		}

		/**
		 * Notificacao do AWT que uma teclado foi usada. Note que
		 * usar uma tecla significa que foi pressionada e liberada.
		 *
		 * @param e Detalhes da tecla usada
		 */
		public void keyTyped(KeyEvent e) {
			// se esta esperando qualquer tecla entao
			// verifica se recebeu alguma recentemente. Pode ocorrer
			// um evento keyType() do usuario liberando as teclas
			// do attack ou dos movimentos, por isso o uso do contador "press_count"
			if (waiting_key)
			{
				if(selecionado == false) return;
				
				if (press_count == 1)
				{
					// agora que o evento de tecla pressionada
					// foi recebido, podemos confirmar ele e iniciar o jogo
					waiting_key = false;
					startGame();
					press_count = 0;
				}
				else
				{
					press_count++;
				}
			}
			
			// se pressionar ESC, sai do jogo
			if (e.getKeyChar() == 27)
			{
				System.exit(0);
			}
		}
	}
	
	/**
	 * O ponto de entrada do jogo. Sera simplesmente criada uma
	 * instancia da classe que vai inicia o display e o loop do jogo
	 * 
	 * @param argv Argumentos passados para o jogo
	 */
	public static void main(String argv[])
	{
		Game g = new Game();

		// Inicia o loop principal, nota: esse metodo nao vai
		// retornar ate que o jogo termine de rodar. Isso porque
		// esta sendo usado o proprio main thread para rodar o jogo
		g.gameLoop();
	}
}
