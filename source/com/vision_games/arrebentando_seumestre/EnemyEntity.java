package com.vision_games.arrebentando_seumestre;

public class EnemyEntity extends Entity
{
	private Game game;
	protected int life;
	protected int attack;
	protected double correct_factor;
	
	public EnemyEntity(Game game,String ref,int x,int y)
	{
		super(ref,x,y);
		
		this.y = 650-sprite.getHeight();

		this.game = game;
		
		correct_factor = 0.0;
	}
	
	/**
	 * Seta a velocidade horizontal dessa entidade
	 * 
	 * @param dx A velocidade horizontal dessa entidade (pixels/sec)
	 */
	@Override
	public void setHorizontalMovement(double dx)
	{
		this.dx = dx;
		
		if(dx < 0)
			sprite.setFlipped(true);
		else if (dx > 0)
			sprite.setFlipped(false);
	}
	

	@Override
	public void moveRight()
	{
		this.dx = max_speed_x*2.0;
		sprite.setFlipped(true);
	}
	
	@Override
	public void moveLeft()
	{
		this.dx = -max_speed_x;
		sprite.setFlipped(false);
	}
	
	public void correctSpeed(boolean c)
	{
		if(c)
			correct_factor = -150.0;
		else
			correct_factor = 0.0;
	}
	
	@Override
	public void move(long delta)
	{
		// atualiza a posicao da entidade baseado na velocidade
		x += (delta * (dx + correct_factor)) / 1000;
		y += (delta * dy) / 1000;
	}
	
	@Override
	public void doLogic()
	{}

	public void collidedWith(Entity other)
	{
	}

	public void deathAnimation()
	{
		
	}
}