package com.vision_games.arrebentando_seumestre;

public class ComputerEntity extends EnemyEntity
{
	private Game game;
	private int count;
	private boolean attacked;
	private boolean powered;
	
	public ComputerEntity(Game game,int x,int y)
	{
		super(game,"resource/image/enemies/computador.png",x,y);
		
		this.y = 650-sprite.getHeight();
		
		this.game = game;

		count = 0;
		life = 1;
		attack = 1;
		powered = false;
		attacked = false;
		max_speed_x = 80.0;
		
		game.updateLogic();
	}
	
	public void move(long delta)
	{
		super.move(delta);
	}
	
	public void takeDamage(int d)
	{
		life -= d;

		if(life == 0)
			game.removeEntity(this);
	}

	public void collidedWith(Entity other)
	{
		if(other instanceof HeroEntity)
		{
			if(((HeroEntity) other).getAttack() == true)
				this.takeDamage(((HeroEntity) other).getDamage());
			else
			{
				if(attacked == false)
				{
					attacked = true;
					if(((HeroEntity) other).getEnergy() > 0)
						powered = true;
					else
					{
						((HeroEntity) other).takeDamage(powered ? (attack+1) : attack);
						powered = false;
					}
				}
			}
			
			this.moveRight();
			count = 200;
			game.updateLogic();
		}
	}
	
	@Override
	public void doLogic()
	{
		if(count == 0)
		{
			attacked = false;
			this.moveLeft();//this.setHorizontalMovement(-300.0f*0.2f);
		}
		else
		{
			game.updateLogic();
			--count;
		}
	}
}