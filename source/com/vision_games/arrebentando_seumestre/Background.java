package com.vision_games.arrebentando_seumestre;

import java.awt.Graphics;

/**
 * Entendidade que representa o plano de fundo
 */
public class Background extends Entity
{
	/** O jogo em que a entidade se encontra */
	private Game game;
	
	/** Sprite secundario para scroll infinito*/ 
	private Sprite sprite_2;
	private boolean bg_order;
	/**
	 * Cria uma nova entidade que representa o plano de fundo
	 *  
	 * @param game O jogo em que o background esta sendo criado
	 * @param ref The reference to the sprite to show for the ship
	 * @param x A posicao x inicial do plano de fundo
	 * @param y A posicao y inicial do plano de fundo
	 */
	public Background(Game game,String ref,int x,int y)
	{
		super(ref,x,y);
		
		sprite_2 = new Sprite(ref);
		
		this.game = game;
		
		bg_order = true;
		max_speed_x = 200.0;
	}

	public Background()
	{
		super("caminho_bg",0,0);
		sprite_2 = new Sprite("caminho_bg2");

		bg_order = true;
	}
	
	/**
	 * Requisita que o cenario se mova baseado numa quantida de tempo
	 * decorrido
	 * 
	 * @param delta O tempo que passou desde o ultimo movimento
	 */
	public void move(long delta)
	{
		if(bg_order == true)
		{
			if(x+sprite.getWidth() <= 5)
			{
				bg_order = false;
				x += sprite.getWidth();
			}
		}
		else
		{
			if(x+sprite_2.getWidth() <= 5)
			{
				bg_order = true;
				x += sprite_2.getWidth();
			}
		}
		
		super.move(delta);
	}
	
	/**
	 * Seta a velocidade horizontal dessa entidade
	 * 
	 * @param dx A velocidade horizontal dessa entidade (pixels/sec)
	 */
	@Override
	public void setHorizontalMovement(double dx)
	{
		this.dx = dx;
	}
	
	@Override
	public void draw(Graphics g)
	{
		if(bg_order == true)
		{
			sprite.draw(g,(int) x,(int) y);
			sprite_2.draw(g,(int) x+sprite.getWidth(),(int) y);
		}
		else
		{
			sprite_2.draw(g,(int) x,(int) y);
			sprite.draw(g,(int) x+sprite_2.getWidth(),(int) y);
		}
	}
	
	@Override
	public void doLogic()
	{}
	
	public void collidedWith(Entity other){}
}