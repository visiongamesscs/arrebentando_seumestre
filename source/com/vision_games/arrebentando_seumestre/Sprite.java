package com.vision_games.arrebentando_seumestre;

import java.awt.Image;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import java.awt.Graphics;

public class Sprite
{
    protected int width;
    protected int height;
    protected boolean flipped;
    protected Image image;

    public Sprite()
    {
        this.flipped = false;
    }

    public Sprite(String img_name)
    {
        this.flipped = false;

        loadImage(img_name);
    }

    protected void getImageDimensions()
    {

        width = image.getWidth(null);
        height = image.getHeight(null);
    }

    protected void loadImage(String imageName)
    {

        ImageIcon ii = new ImageIcon(imageName);
        image = ii.getImage();
        
        this.getImageDimensions();
    }

    public Image getImage()
    {
        return image;
    }

    /**
     * Pega a largura do sprite desenhado
     * 
     * @return A largura em pixels desse sprite
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Pega a altura do sprite desenhado
     * 
     * @return A altura em pixels desse sprite
     */
    public int getHeight()
    {
        return height;
    }
    
    public void setFlipped(Boolean f)
    {
    	this.flipped = f;
    }
    
    public boolean isFlipped()
    {
    	return flipped;
    }

    /**
     * Desenha o sprite no contexto grafico dado
     * 
     * @param g O contexto grafico no qual o sprite sera desenhado
     * @param x A posicao x na qual o sprite sera desenhado
     * @param y A posicao y na qual o sprite sera desenhado
     */
	public void draw(Graphics g,int x,int y)
    {
		if(flipped)
			g.drawImage(image,x+width,y,-width,height,null);
		else
			g.drawImage(image,x,y,width,height,null);
		
	}
}