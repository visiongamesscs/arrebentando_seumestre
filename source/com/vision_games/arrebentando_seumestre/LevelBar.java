package com.vision_games.arrebentando_seumestre;

import java.awt.Color;
import java.awt.Graphics;

public class LevelBar extends Entity
{
	private Game game;
	private Sprite empty;
	private int max;
	private int level;
	
	public LevelBar(Game game,String full,String empty_s,int x,int y,int max)
	{
		super(full,x,y);
		
		empty = new Sprite(empty_s);
		
		this.max = max;
		this.level = max;
		
		this.game = game;
	}
	
	public void reset()
	{
		level = max;
	}
	
	public void setLevel(int l)
	{
		if(l <= max)
			level = l;
	}
	
	public int getLevel()
	{
		return level;
	}

	public void upLevel()
	{
		if(level < max) ++level;
	}
	
	public void downLevel()
	{
		if(level > 0) --level;
	}
	
	@Override
	public void draw(Graphics g)
	{
		for(int i = 0; i < max; ++i)
		{
			if(i < level)
				sprite.draw(g,(int) x + (40*i),(int) y);
			else
				empty.draw(g,(int) x + (40*i),(int) y);
		}
		
		String message = Integer.toString(max)+"/"+Integer.toString(level);
		
		g.setColor(Color.black);
		g.drawString(message,(int)x,(int)y+sprite.getHeight()+g.getFontMetrics().getHeight()+5);
	}
	
	@Override
	public void setHorizontalMovement(double dx)
	{
		
	}
	
	public void collidedWith(Entity other)
	{
		
	}
	
	public void doLogic()
	{
		
	}
}
